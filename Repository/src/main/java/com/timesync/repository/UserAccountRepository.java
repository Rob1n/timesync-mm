package com.timesync.repository;

import com.timesync.dao.UserAccountDAO;
import org.springframework.data.repository.CrudRepository;

public interface UserAccountRepository extends CrudRepository<UserAccountDAO, Long> {
}
