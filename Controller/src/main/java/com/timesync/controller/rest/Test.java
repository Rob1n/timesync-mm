package com.timesync.controller.rest;

import com.timesync.json.TestJson;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Test {

	@RequestMapping("/home")
	public TestJson getHome() {
		return new TestJson(5L, "Hello world!");
	}

}
